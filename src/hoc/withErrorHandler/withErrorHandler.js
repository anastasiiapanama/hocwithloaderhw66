import React, {useEffect, useState} from 'react';
import Modal from "../../components/UI/Modal/Modal";
import Spinner from "../../components/UI/Spinner/Spinner";

const withErrorHandler = (WrappedComponent, axios) => {
    return function WithErrorHandler(props) {
        const [loading, setLoading] = useState(false);
        const [error, setError] = useState(null);

        useEffect(() => {
            axios.interceptors.request.use(req => {
                setLoading(true);
                return req;
            });
        }, []);

        useEffect(() => {
            axios.interceptors.response.use(res => {
                setLoading(false);
                return res;
            }, error => {
                setLoading(false);
                setError(error);
                throw error;
            });
        }, []);

        const errorDismissed = () => {
            setError(null);
        };

        return (
            <>
                {loading && <Spinner/>}

                {error && <Modal show={!!error} closed={errorDismissed}>
                    {error && (error.message || "Network error")}
                </Modal>}
                <WrappedComponent {...props}/>
            </>
        );
    };
};

export default withErrorHandler;