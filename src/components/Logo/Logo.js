import React from 'react';
import LogoImg from '../../assets/images/logo.png';
import './Logo.css';

const Logo = () => {
    return (
        <div className="Logo">
            <img src={LogoImg} alt="Flora"/>
        </div>
    );
};

export default Logo;