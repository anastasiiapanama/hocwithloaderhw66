import React, {useEffect, useState} from 'react';
import Grid from "@material-ui/core/Grid";
import makeStyles from "@material-ui/core/styles/makeStyles";
import MenuItem from "@material-ui/core/MenuItem";
import Paper from "@material-ui/core/Paper";
import TextField from "@material-ui/core/TextField";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import axiosContent from "../../axios-content";

const useStyles = makeStyles(theme => ({
    paper: {
        padding: theme.spacing(2)
    }
}));

const Admin = props => {
    const classes = useStyles();
    const [pagesList, setPagesList] = useState([]);
    const [page, setPage] = useState('');
    const [pageData, setPageData] = useState({});

    useEffect(() => {
        const getList = async () => {
            try {
                const list = await axiosContent.get('pages.json');
                const array = Object.keys(list.data);
                setPagesList(array);
            } catch(e) {
                console.log(e);
            }
        }

        getList().then(e => 'error');
    }, [])

    useEffect(() => {
        const getPageData = async () => {
            try {
                const response = await axiosContent.get(`pages/${page}.json`);
                setPageData(response.data);
            } catch (e) {
                console.log(e);
            }
        }
        getPageData().then(e => 'error');
    }, [page])

    const editPage = async event => {
        event.preventDefault();
        const editContent = {
            category: pageData.category,
            title: pageData.title,
            content: pageData.content
        }

        try {
            await axiosContent.put('pages/' + page + '.json', editContent);
        } finally {
            props.history.push('/');
        }
    };

    const changeContent = event => {
        const {name, value} = event.target;
        setPage(value)
    };

    const changeHandler = (e) => {
        setPageData({...pageData, [e.target.name]: e.target.value});
    }

    return (
        <Paper className={classes.paper}>
            <form onSubmit={editPage}>
                <Grid container direction="column" spacing={2}>
                    <Grid item xs>
                        <Typography variant="h5">Edit pages</Typography>
                    </Grid>
                    <Grid item xs>
                        <TextField
                            variant="outlined"
                            fullWidth
                            select
                            label="Category"
                            name="category"
                            value={page}
                            required
                            onChange={changeContent}
                        >
                            {pagesList.map(c => (
                                <MenuItem key={c} value={c}>{c}</MenuItem>
                            ))}
                        </TextField>
                    </Grid>
                    <Grid item xs>
                        <TextField
                            variant="outlined"
                            fullWidth
                            type="text"
                            name="title"
                            placeholder="Title"
                            value={pageData.title}
                            required
                            onChange={changeHandler}
                        />
                    </Grid>
                    <Grid item xs>
                        <TextField
                            variant="outlined"
                            fullWidth
                            type="content"
                            name="content"
                            placeholder="Content"
                            value={pageData.content}
                            required
                            onChange={changeHandler}
                            multiline
                            rows={5}
                        />
                    </Grid>
                    <Grid item xs>
                        <Button type="submit" variant="contained" color="primary">Edit</Button>
                    </Grid>
                </Grid>
            </form>
        </Paper>
    );
};

export default Admin;