import React, {useEffect, useState} from 'react';
import PaperWithPadding from "../../components/UI/PaperWithPadding";
import axiosContent from "../../axios-content";
import Grid from "@material-ui/core/Grid";
import withErrorHandler from "../../hoc/withErrorHandler/withErrorHandler";

const Pages = ({match}) => {
    const [page, setPage] = useState([]);

    useEffect(() => {
        const fetchData = async () => {
            let url = `pages/${match.params.name}.jso`;

            const response = await axiosContent.get(url);
            console.log(response.data);

            setPage(response.data);
        };

        fetchData().catch(console.error);
    }, [match.params.name]);

    return (
        page && <Grid container direction="column" spacing={2}>
            <Grid item xs>
                <PaperWithPadding>
                    <h2>{page.title}</h2>
                    <p>{page.content}</p>
                </PaperWithPadding>
            </Grid>
        </Grid>
    );
};


export default withErrorHandler(Pages, axiosContent);