export const CATEGORIES = [
    {title: 'News', id: 'news'},
    {title: 'Orders', id: 'orders'},
    {title: 'Our gallery', id: 'gallery'},
    {title: 'About', id: 'about'},
    {title: 'Contacts', id: 'contacts'}
]