import axios from "axios";

const axiosContent = axios.create({
    baseURL: 'https://propject-burger-ponamareva-default-rtdb.firebaseio.com/'
});

export default axiosContent;