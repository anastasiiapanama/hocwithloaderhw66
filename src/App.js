import './App.css';
import Layout from "./components/Layout/Layout";
import {Route, Switch} from "react-router-dom";
import Pages from "./containers/Pages/Pages";
import Admin from "./containers/Admin/Admin";

const App = () => (
    <Layout>
      <Switch>
        <Route path="/" exact component={Pages} />
        <Route path="/pages/admin" exact component={Admin} />
        <Route path="/pages/:name" component={Pages} />
        <Route render={() => <h1>Not found</h1>} />
      </Switch>
    </Layout>
)

export default App;
